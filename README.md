This is a Hamiltonian Monte Carlo implementation based on the NIFTy framework.

### Requirements

- [Python 3](https://www.python.org/) (3.6.x or later)
- [NIFTy6](https://gitlab.mpcdf.mpg.de/ift/nifty)

Optional dependencies:
- [mpi4py](https://mpi4py.scipy.org) (for MPI-parallel execution)
- [matplotlib](https://matplotlib.org/) (for  plotting)

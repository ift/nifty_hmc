# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2020 Max-Planck-Society
#
# NIFTy_HMC is being developed at the Max-Planck-Institut fuer Astrophysik.

from setuptools import setup

exec(open('nifty_hmc/version.py').read())

setup(name="nifty_hmc",
      version=__version__,
      author="Jakob Knollmueller",
      author_email="jakob@mpa-garching.mpg.de",
      description="",
      url="https://gitlab.mpcdf.mpg.de/ift/nifty_hmc",
      packages=["nifty_hmc"],
      zip_safe=True,
      dependency_links = [],
      install_requires = ["nifty6>=6.0"],
      license="GPLv3",
      classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 "
        "or later (GPLv3+)"
    ],)
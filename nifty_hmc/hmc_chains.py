# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2020 Max-Planck-Society
#
# NIFTy_HMC is being developed at the Max-Planck-Institut fuer Astrophysik.

import numpy as np
import nifty6 as ift
from .diagnostics import get_ESS


class HMC_chain:
    """ The class for individual chains to perform the Hamiltonian Monte Carlo sampling.

    Parameters
    -----------
    V: EnergyOperator
        The problem Hamiltonian, used as potential energy in the Hamiltonian Dynamics of HMC.
    position:  Fields/MultiFields
        The position the chains are initialized.
    M: DiagonalOperator
        The mass matrix for the momentum term in the Hamiltonian dynamics.
        If not set, a unit matrix is assumed. Default: None
    steplength: Float
        The length of the steps in the leapfrog integration. This should be tuned
        to achieve reasonable acceptance for the given problem. Default: 0.003
    steps: positive Integer
        The number of leapfrog integration steps for the next sample. Default: 10
    """

    def __init__(self, V, position, M=None, steplength=0.003, steps=10, sseq=None):
        if sseq is None:
            raise RuntimeError

        if M is None:
            M = ift.ScalingOperator(position.domain, 1)
        self._position = position
        self.samples = []
        self._M = M
        self._V = V
        self._steplength = steplength
        self._steps = steps
        self._energies = []
        self._accepted = []
        self._current_acceptance =[]
        self._sseq = sseq

    def sample(self,N):
        """ The method to draw a set of samples.

        Parameters
        -----------
        N: positive Integer
        The number of samples to be drawn.
        """
        for i in range(N):
            self._sample()
            print('iteration: ' + str(i) + ' acceptance: ' +
                  str(self._current_acceptance[-1])+ ' steplength: '+ str(self._steplength))


    def warmup(self,N ,preferred_acceptance=0.6, keep=False):
        """ Performing a warmup by tuning the steplength
         to achieve a certain acceptance rate and estimating the mass matrix.

        Parameters
        -----------
        N: positive Integer
        The number of warmup samples to be drawn.
        preferred_acceptance: Float
        The acceptance rate according to which the stepsize is tuned. Default: 0.6
        keep: Boolean
        Whether to keep the drawn samples or discard them. Default: False
        """
        for i in range(N):
            self._sample()
            self._tune_parameters(preferred_acceptance)
            print('WARMUP: ' + str(i) + ' acceptance: ' +
                  str(self._current_acceptance[-1])+ ' steplength: '+ str(self._steplength))
        sc = ift.StatCalculator()
        for sample in self.samples:
            sc.add(sample)
        self.M = ift.makeOp(sc.var).inverse
        if not keep:
            self.samples = []

    def estimate_quantity(self, function):
        """ Estimates the result of a function over all samples of the chains.

        Parameters
        -----------
        function: Function
        The function to be evaluated and averaged over the samples.

        Returns
        -----------
        mean, var : Tuple
        The mean and variance over the samples.

        """
        sc = ift.StatCalculator()
        for sample in self.samples:
            sc.add(function(sample))
        return sc.mean, sc.var

    def _sample(self):
        """ Draws one sample according to the HMC algorithm.
        """
        tmp = self._sseq.spawn(2)[1]
        with ift.random.Context(tmp):
            momentum = self._M.draw_sample_with_dtype(dtype=np.float64)

        new_position, new_momentum = self._integrate(momentum)
        self._accepting(momentum, new_position, new_momentum)
        self._update_acceptance()


    def _integrate(self, momentum):
        """ Performs the leapfrog integration of the equations of motion.
        Parameters
        -----------
        momentum: Field or Multifield
            The momentum vector in the Hamilton equations.
        """
        position = self._position
        for i in range(self._steps):
            position, momentum = self._leapfrog(position, momentum)
        return position, momentum

    def _leapfrog(self, position, momentum):
        """ Performs one leapfrog integration step.

        Parameters
        -----------
        position: Field or Multifield
            The position vector in the Hamilton equations.
        momentum: Field or Multifield
            The momentum vector in the Hamilton equations.
        """
        lin = ift.Linearization.make_var(position)
        gradient = self._V(lin).gradient
        momentum = momentum - self._steplength/2. * gradient
        position = position + self._steplength * self._M.inverse(momentum)
        lin = ift.Linearization.make_var(position)
        gradient = self._V(lin).gradient
        momentum = momentum - self._steplength/2. * gradient

        return position, momentum

    def _accepting(self, momentum, new_position, new_momentum):
        """ Decides whether to accept or decline a new position according to Metropolis-Hastings.
            The current position is then stored as new sample.

        Parameters
        -----------
        momentum: Field or Multifield
            The old momentum vector in the Hamilton equations.
        new_position: Field or Multifield
            The new position vector after evolving the equations of motion.
        new_momentum: Field or Multifield
            The new momentum vector after evolving the equations of motion.
        """
        energy = self._V(self._position).val + (0.5* momentum.vdot(self._M.inverse(momentum))).val
        new_energy = self._V(new_position).val + (0.5* new_momentum.vdot(self._M.inverse(new_momentum))).val
        if new_energy < energy:
            self._position = new_position
            accept = 1
        else:
            rate = np.exp(energy - new_energy)
            if  np.isnan(rate):
                return
            rng = ift.random.current_rng()
            accept = rng.binomial(1,rate)
            if accept:
                self._position = new_position
        self._accepted.append(accept)
        self.samples.append(self._position)
        self._energies.append(energy)


    def _update_acceptance(self):
        """ Calculates the current acceptance rate based on the last ten samples.
        """
        current_accepted = self._accepted[-10:]
        current_accepted = np.array(current_accepted)
        current_acceptance = np.mean(current_accepted)
        self._current_acceptance.append(current_acceptance)


    def _tune_parameters(self, preferred_acceptance):
        """ Increases or decreases the steplength in the leapfrog integration
            based on the current acceptance rate to aim for the preferred rate.

        Parameters
        -----------
        preferred_acceptance: Float
            The preferred acceptance rate.
        """
        if self._current_acceptance[-1] < preferred_acceptance:
            self._steplength *= 0.99
        else:
            self._steplength *= 1.01


    @property
    def ESS(self):
        """ The effective sample size over all samples of the chain.

        Returns
        -----------
        ESS: MultiField
        The effective sample size of all model parameters of the chain.
        """
        return get_ESS(self.samples)

    def mean(self):
        """ The mean over all samples of the chain.

        Returns
        -----------
        mean: Field or MultiField
        The mean over all samples of the chain.
        """
        return self._mean(self._sample_field())


    def _mean(self, fld):
        result = {}
        dom = self._position.domain
        for key in fld.keys():
            mean = fld[key].val.mean(axis=-1)
            result[key[:-2]] = ift.Field.make(dom[key[:-2]],mean)
        result = ift.MultiField.from_dict(result, self._position.domain)
        return result

    def _var(self, fld):
        result = {}
        dom = self._position.domain
        for key in fld.keys():
            var = fld[key].val.var(axis=-1)
            result[key[:-2]] = ift.Field.make(dom[key[:-2]],var)
        result = ift.MultiField.from_dict(result,self._position.domain)
        return result


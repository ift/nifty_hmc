# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2020 Max-Planck-Society
#
# NIFTy_HMC is being developed at the Max-Planck-Institut fuer Astrophysik.

import numpy as np
import nifty6 as ift
from .operators import ACF_Selector


# Effective Sample Size
def get_ESS(samples):
    """ The effective sample size over a set of samples.

    Returns
    -----------
    ESS: MultiField
    The effective sample size for all parameters in the samples.
    """
    result = {}
    autocorrelations = get_AFC(samples)
    for key in autocorrelations.keys():
        fld = autocorrelations[key].val
        addaxis=False
        if len(fld.shape) == 1:
            # empty domaintuple war weird
            fld = fld.reshape((1,)+fld.shape)
            addaxis=True
        cum_field = np.cumsum(fld ,axis=-1)
        correlation_length = np.argmax(fld <0, axis=-1)
        indices = np.where(np.ones(cum_field[... ,0].shape))
        indices += ( correlation_length.flatten() -1,)
        integr_corr = cum_field[indices] -1
        ESS = len(samples) /(1 + 2* integr_corr)
        if addaxis:
            result[key[:-2]] = ift.Field(samples[0].domain[key[:-2]], ESS[0])
        else:
            result[key[:-2]] = ift.Field(samples[0].domain[key[:-2]], ESS.reshape(correlation_length.shape))
    return ift.MultiField.from_dict(result)


# AutoCorrelationFunction
def get_AFC(samples):
    """ Calculates the auto-correlation function for every parameter in the samples.

    Returns
    -----------
    result: MultiField
    The auto-correlation function for every parameter in the samples
    """
    sample_field = _standardized_sample_field(samples)
    result = {}
    for key in sample_field.keys():
        AFC = ACF_Selector(sample_field[key].domain, len(samples))
        FFT = ift.FFTOperator(sample_field[key].domain, space=len(sample_field[key].domain._dom) - 1)
        h = FFT(sample_field[key])
        hch = h.conjugate() * h
        autocorr = FFT.inverse(hch)
        result[key] = AFC(autocorr).real
    return result


def _mean(fld, dom):
    result = {}
    for key in fld.keys():
        mean = fld[key].val.mean(axis=-1)
        result[key[:-2]] = ift.makeField(dom[key[:-2]],mean)
    result = ift.MultiField.from_dict(result, dom)
    return result

def _var(fld, dom):
    result = {}
    for key in fld.keys():
        var = fld[key].val.var(axis=-1)
        result[key[:-2]] = ift.makeField(dom[key[:-2]],var)
    result = ift.MultiField.from_dict(result,dom)
    return result

def _standardized_sample_field(samples):
    di={}
    dom = samples[0].domain
    fld = _sample_field(samples)
    mean = _mean(fld,dom)
    var = _var(fld,dom)
    for key in dom.keys():
        sub_fld = fld[key+'_t']
        sub_dom = sub_fld.domain
        sub_fld = (sub_fld.val - mean[key].val[..., np.newaxis])/(var[key].val**0.5)[..., np.newaxis]
        di[key+'_t'] = ift.makeField(sub_dom, sub_fld)
    return di


def _sample_field(samples):
    di={}
    time_domain = ift.RGSpace(len(samples))
    dom = samples[0].domain
    for key in dom.keys():
        fld = np.empty(dom[key].shape + time_domain.shape)
        fld_dom = ift.DomainTuple.make(dom[key]._dom + (time_domain,))
        for i in range(time_domain.shape[0]):
            fld[...,i] = (samples[i][key]).val
        di[key+'_t'] = ift.makeField(fld_dom, fld)
    return di

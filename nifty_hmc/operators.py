# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2020 Max-Planck-Society
#
# NIFTy_HMC is being developed at the Max-Planck-Institut fuer Astrophysik.

import nifty6 as ift



class ACF_Selector(ift.LinearOperator):
    def __init__(self, domain, N_samps):
        self._domain = domain
        self._N_samps = N_samps
        us_dom = ift.UnstructuredDomain(self._N_samps//2)
        self._target = ift.DomainTuple.make(self.domain._dom[:-1]+ (us_dom,))
        self._capability = self.TIMES

    def apply(self, x, mode):
        if mode == self.TIMES:
            result = x.val[...,:self._N_samps//2]
        return ift.makeField(self._target ,result)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2020 Max-Planck-Society
#
# NIFTy_HMC is being developed at the Max-Planck-Institut fuer Astrophysik.


import numpy as np
import nifty6 as ift
from .hmc_chains import HMC_chain


class HMC_Sampler:
    """ The sampler class, managing chains and the computations of diagnostics.

    Parameters
    -----------
    V: EnergyOperator
        The problem Hamiltonian, used as potential energy in the Hamiltonian Dynamics of HMC.
    initial_position: List of Fields/MultiFields
        The position the chains are initialized.
    chains: positive Integer
        The number of chains. Default: 1
    M: DiagonalOperator
        The mass matrix for the momentum term in the Hamiltonian dynamics.
        If not set, a unit matrix is assumed. Default: None
    steplength: Float
        The length of the steps in the leapfrog integration. This should be tuned
        to achieve reasonable acceptance for the given problem. Default: 0.003
    steps: positive Integer
        The number of leapfrog integration steps for the next sample. Default: 10
    """

    def __init__(self, V, initial_position, chains=1, M=None, steplength=0.003, steps=10, comm=None):
        self._M = M
        self._V = V
        self._dom = initial_position[0].domain  # FIXME temporary!
        self._steplength = steplength
        self._steps = steps
        self._N_chains = chains
        sseq = ift.random.spawn_sseq(self._N_chains)
        self._local_chains = []
        self._comm = comm
        ntask, rank, _ = ift.utilities.get_MPI_params_from_comm(self._comm)
        lo, hi = ift.utilities.shareRange(self._N_chains, ntask, rank)
        for i in range(lo, hi):
            self._local_chains.append(HMC_chain(self._V, initial_position[i], self._M,
                                                self._steplength, self._steps, sseq[i]))


    def sample(self, N):
        """ The method to draw a set of samples in every chain.

        Parameters
        -----------
        N: positive Integer
        The number of samples to be drawn in every chain.
        """
        for chain in self._local_chains:
            chain.sample(N)

    def warmup(self,N, preferred_acceptance=0.6, keep=False):
        """ Performing a warmup by tuning the steplength
         to achieve a certain acceptance rate and estimating the mass matrix.

        Parameters
        -----------
        N: positive Integer
        The number of warmup samples to be drawn in every chain.
        preferred_acceptance: Float
        The acceptance rate according to which the stepsize is tuned. Default: 0.6
        keep: Boolean
        Whether to keep the drawn samples or discard them. Default: False
        """
        for chain in self._local_chains:
            chain.warmup(N, preferred_acceptance, keep)

    def estimate_quantity(self, function):
        """ Estimates the result of a function over all samples and chains.

        Parameters
        -----------
        function: Function
        The function to be evaluated and averaged over the samples.

        Returns
        -----------
        mean, var : Tuple
        The mean and variance over the samples.

        """
        locmeanvar = [chain.estimate_quantity(function) for chain in self._local_chains]
        locmean = [x[0] for x in locmeanvar]
        locvar = [x[1] for x in locmeanvar]
        mean = ift.utilities.allreduce_sum(locmean, self._comm)
        var = ift.utilities.allreduce_sum(locvar, self._comm)
        return mean/self._N_chains, var/self._N_chains

    @property
    def ESS(self):
        """ The effective sample size over all samples and chains.

        Returns
        -----------
        ESS: MultiField
        The effective sample size of all model parameters.
        """
        locESS = [chain.ESS for chain in self._local_chains]
        return ift.utilities.allreduce_sum(locESS, self._comm)


    @property
    def R_hat(self):
        """ The Gelman-Rubin test statistic R_hat.

        It measures how well the samples of different chains agree to determine convergence.
        Ideally this quantity is close to unity.

        Returns
        -----------
        R_hat: Field or MultiField
        The value of R_hat for all model parameters.
        """
        from .diagnostics import _sample_field, _mean, _var
        ntask, rank, master = ift.utilities.get_MPI_params_from_comm(self._comm)
        N = len(self._local_chains[0].samples) if master else None
        if ntask > 1:
            N = self._comm.bcast(N, root=0)
        M = self._N_chains
        dom = self._dom
        locfld = [_sample_field(chain.samples) for chain in self._local_chains]
        locmeanmean = [_mean(fld,dom) for fld in locfld]
        locW = [_var(fld,dom) for fld in locfld]
        mean_mean = ift.utilities.allreduce_sum(locmeanmean, self._comm) / M
        W = ift.utilities.allreduce_sum(locW, self._comm) / M

        locB = [(mean_mean-_mean(fld,dom))**2 for fld in locfld]
        B = ift.utilities.allreduce_sum(locB, self._comm) * N / (M - 1)

        var_theta = (1 - 1 / N) * W + (M+1) / (N * M) * B
        R_hat = ift.sqrt(var_theta / W)
        return R_hat



